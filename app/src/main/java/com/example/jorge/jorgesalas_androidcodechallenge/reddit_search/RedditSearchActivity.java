package com.example.jorge.jorgesalas_androidcodechallenge.reddit_search;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.widget.Button;
import android.widget.Toast;

import com.example.jorge.jorgesalas_androidcodechallenge.R;
import com.example.jorge.jorgesalas_androidcodechallenge.constants.GeneralConstants;
import com.example.jorge.jorgesalas_androidcodechallenge.models.RedditPost;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RedditSearchActivity extends AppCompatActivity implements RedditSearchMvpView {

    @Bind(R.id.search_view)
    SearchView searchView;
    @Bind(R.id.post_recycler)
    RecyclerView redditPostRecyclerView;
    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.blue_search_button)
    Button searchButton;

    private RedditPostAdapter adapter = new RedditPostAdapter();

    private RedditSearchPresenter presenter = new RedditSearchPresenter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reddit_search);
        ButterKnife.bind(this);
        presenter.attachView(this);
        setUpSwipeRefreshLayout();
        setUpRedditPostRecyclerView();
        customizeSearchButtonFont();
        // Sets up the default search subcategory and triggers the search itself.
        searchView.setQuery(presenter.getDefaultSearchSubcategory(), false);
        search();
    }

    private void setUpRedditPostRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        redditPostRecyclerView.setLayoutManager(linearLayoutManager);
        redditPostRecyclerView.setAdapter(adapter);
    }

    private void setUpSwipeRefreshLayout() {
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.repeatLastSearch();
            }
        });
    }

    @Override
    public void showEmptySearchCriteriaError() {
        Toast.makeText(this, R.string.reddit_search_empty_search_criteria_error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void updateRedditPosts(List<RedditPost> newPosts) {
        adapter.setNewPostList(newPosts);
        hideRefreshingProgressBar();
    }

    @Override
    public void showNoResultsFoundError() {
        Toast.makeText(this, R.string.reddit_search_no_results_error, Toast.LENGTH_LONG).show();
        hideRefreshingProgressBar();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    /**
     * Queries the reddit api to retrieve post information from the subcategory specified in the
     * search view.
     */
    @OnClick(R.id.blue_search_button)
    public void search() {
        presenter.searchPostsInSubCategory(searchView.getQuery().toString());
    }

    /**
     * Hides the circle progress bar shown when the pull to refresh action was used.
     */
    private void hideRefreshingProgressBar() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void customizeSearchButtonFont () {
        Typeface authorFont = Typeface.createFromAsset(getAssets(),
                GeneralConstants.BEBASNEUE_FONT_PATH);
        searchButton.setTypeface(authorFont);
    }
}
