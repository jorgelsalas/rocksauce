package com.example.jorge.jorgesalas_androidcodechallenge.rest;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

/**
 * Helps ignore unnecessary data from JSON response.
 */
public class ItemTypeAdapterFactory implements TypeAdapterFactory{

    public <T> TypeAdapter<T> create(Gson gson, final TypeToken<T> type) {

        final TypeAdapter<T> delegate = gson.getDelegateAdapter(this, type);
        final TypeAdapter<JsonElement> elementAdapter = gson.getAdapter(JsonElement.class);

        return new TypeAdapter<T>() {

            public void write(JsonWriter out, T value) throws IOException {
                delegate.write(out, value);
            }

            /**
             * This method filters out irrelevant information from the json response provided by
             * Reddit's api. We are only interested in the list represented by the "children" tag,
             * so the "outer" content of the response is filtered out by this method.
             *
             * @param in A reader to the json response from Reddit's api.
             * @return The filtered response from Reddit's api.
             * @throws IOException
             */
            public T read(JsonReader in) throws IOException {

                JsonElement jsonElement = elementAdapter.read(in);
                if (jsonElement.isJsonObject()) {
                    JsonObject jsonObject = jsonElement.getAsJsonObject();
                    if(jsonObject.has(APIConstants.DATA_TAG)) {
                        JsonElement data = jsonObject.get(APIConstants.DATA_TAG);

                        jsonObject = data.getAsJsonObject();
                        if (jsonObject.has(APIConstants.REDDIT_POST_PARENT_TAG) &&
                                (jsonObject.get(APIConstants.REDDIT_POST_PARENT_TAG).isJsonObject() ||
                                        jsonObject.get(APIConstants.REDDIT_POST_PARENT_TAG).isJsonArray())) {

                            jsonElement = jsonObject.get(APIConstants.REDDIT_POST_PARENT_TAG);
                        }
                    }
                }

                return delegate.fromJsonTree(jsonElement);
            }
        }.nullSafe();
    }

}
