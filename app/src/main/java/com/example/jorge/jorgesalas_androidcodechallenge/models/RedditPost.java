package com.example.jorge.jorgesalas_androidcodechallenge.models;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

/**
 * Model that stores the Reddit Post information that will be displayed to users.
 */
public class RedditPost {

    public RedditPostData data;

    public RedditPost() {

    }

    @Override
    public String toString() {
        Type type = new TypeToken<RedditPost>(){}.getType();
        return new Gson().toJson(this, type);
    }

    public class RedditPostData {
        public String title = "";
        public String author = "";
        public String thumbnail = "";
        @SerializedName("num_comments")
        public int numberOfComments;
        public int ups;
        public int downs;

        public RedditPostData() {

        }

        @Override
        public String toString() {
            Type type = new TypeToken<RedditPostData>(){}.getType();
            return new Gson().toJson(this, type);
        }
    }
}
