package com.example.jorge.jorgesalas_androidcodechallenge.reddit_search;

import com.example.jorge.jorgesalas_androidcodechallenge.base.MvpView;
import com.example.jorge.jorgesalas_androidcodechallenge.models.RedditPost;

import java.util.List;

/**
 * Concrete interface for the reddit search activity
 */
public interface RedditSearchMvpView extends MvpView {

    /**
     * Shows an error message when the user attempts a search without having entered a subcategory.
     */
    void showEmptySearchCriteriaError();

    /**
     * Updates the UI to show the newly fetched posts from Reddit's api.
     *
     * @param newPosts the new posts that will be displayed.
     */
    void updateRedditPosts(List<RedditPost> newPosts);

    /**
     * Shows an error indicating no results where found when querying Reddit's api.
     */
    void showNoResultsFoundError();
}
