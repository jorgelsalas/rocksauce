package com.example.jorge.jorgesalas_androidcodechallenge.reddit_search;

import android.util.Log;

import com.example.jorge.jorgesalas_androidcodechallenge.base.BasePresenter;
import com.example.jorge.jorgesalas_androidcodechallenge.models.RedditPost;
import com.example.jorge.jorgesalas_androidcodechallenge.rest.APIClient;
import com.example.jorge.jorgesalas_androidcodechallenge.rest.APIConstants;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Presenter for the Reddit Search Activity.
 */
public class RedditSearchPresenter extends BasePresenter<RedditSearchMvpView> {

    private static String TAG = RedditSearchPresenter.class.getSimpleName();

    private String mLastSearchSubcategory = APIConstants.FUNNY_SUBCATEGORY;

    public void searchPostsInSubCategory(String subcategory) {
        if(subcategory.isEmpty()) {
            getMvpView().showEmptySearchCriteriaError();
        }
        else {
            mLastSearchSubcategory = subcategory;
            Call<List<RedditPost>> call = APIClient.getRedditServiceInstance().fetchPostFromSubcategory(subcategory);
            call.enqueue(new Callback<List<RedditPost>>() {
                @Override
                public void onResponse(Call<List<RedditPost>> call, Response<List<RedditPost>> response) {
                    if (response.code() == APIConstants.SUCCESS_CODE && null != response.body()
                            && response.body().size() > 0) {
                        getMvpView().updateRedditPosts(response.body());
                        Log.e(TAG, response.body().toString());
                    }
                    else {
                        getMvpView().showNoResultsFoundError();
                    }
                }

                @Override
                public void onFailure(Call<List<RedditPost>> call, Throwable t) {
                    getMvpView().showNoResultsFoundError();
                }
            });
        }
    }

    public void repeatLastSearch() {
        searchPostsInSubCategory(mLastSearchSubcategory);
    }

    public String getDefaultSearchSubcategory() {
        return APIConstants.FUNNY_SUBCATEGORY;
    }

}
