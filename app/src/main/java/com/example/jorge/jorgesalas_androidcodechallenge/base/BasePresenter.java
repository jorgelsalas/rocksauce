package com.example.jorge.jorgesalas_androidcodechallenge.base;

/**
 * Base implementation of the Presenter interface. Handles attaching and detaching the MvpView as
 * well as providing access to it via the getMvpView method.
 */
public class BasePresenter<T extends MvpView> implements Presenter<T> {

    private T mMvpView;

    @Override
    public void attachView(T mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void detachView() {
        mMvpView = null;
    }

    @Override
    public boolean isViewAttached() {
        return mMvpView != null;
    }

    @Override
    public T getMvpView() {
        return mMvpView;
    }


}
