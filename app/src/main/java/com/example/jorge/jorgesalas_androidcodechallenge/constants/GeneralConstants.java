package com.example.jorge.jorgesalas_androidcodechallenge.constants;

/**
 * Constants that may be used in more than one module of the app.
 */
public class GeneralConstants {
    public static final String BEBASNEUE_FONT_PATH = "fonts/bebasneue.ttf";
}
