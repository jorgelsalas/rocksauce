package com.example.jorge.jorgesalas_androidcodechallenge.base;

/**
 * Interface that defines the base contract MvpViews must fulfill. Specific views will have their
 * own interface that will in turn implement this base interface.
 */
public interface MvpView {

}
