package com.example.jorge.jorgesalas_androidcodechallenge.base;

/**
 * Interface that all presenters must conform to. Alternatively a presenter may extend the
 * {@link BasePresenter} class.
 */
public interface Presenter<V extends MvpView> {

    void attachView(V mvpView);
    void detachView();
    boolean isViewAttached();
    V getMvpView();
}
