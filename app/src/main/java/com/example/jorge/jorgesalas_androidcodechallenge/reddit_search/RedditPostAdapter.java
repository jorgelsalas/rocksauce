package com.example.jorge.jorgesalas_androidcodechallenge.reddit_search;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jorge.jorgesalas_androidcodechallenge.R;
import com.example.jorge.jorgesalas_androidcodechallenge.constants.GeneralConstants;
import com.example.jorge.jorgesalas_androidcodechallenge.models.RedditPost;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Adapter that handles transition from API received post data to their UI representation.
 */
public class RedditPostAdapter extends RecyclerView.Adapter<RedditPostAdapter.RedditPostHolder> {

    List<RedditPost> posts = new ArrayList<>();

    public static class RedditPostHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.thumbnail)
        CircleImageView thumbnail;
        @Bind(R.id.reddit_post_author)
        TextView authorTextView;
        @Bind(R.id.reddit_post_title)
        TextView titleTextView;
        @Bind(R.id.reddit_post_comment_number)
        TextView commentsTextView;
        @Bind(R.id.reddit_post_up_votes)
        TextView upVotesTextView;
        @Bind(R.id.reddit_post_down_votes)
        TextView downVotesTextView;

        public RedditPostHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            v.setOnClickListener(this);
            setAuthorFont();
        }

        public void bindRedditPost(RedditPost post) {
            Context context = itemView.getContext();
            Resources resources = context.getResources();
            authorTextView.setText(post.data.author);
            titleTextView.setText(post.data.title);

            commentsTextView.setText(resources.getQuantityString(R.plurals.comments,
                    post.data.numberOfComments, post.data.numberOfComments));
            upVotesTextView.setText(resources.getQuantityString(R.plurals.up_votes,
                    post.data.ups, post.data.ups));
            downVotesTextView.setText(resources.getQuantityString(R.plurals.down_votes,
                    post.data.downs, post.data.downs));

            // The noFade option is used to prevent unexpected behavior as specified in the
            // CircleImageView library documentation.
            if(!post.data.thumbnail.isEmpty()) {
                Picasso.with(itemView.getContext()).load(post.data.thumbnail)
                        .placeholder(R.drawable.rocksauce_logo).noFade().into(thumbnail);
            }
            animatePost();
        }

        @Override
        public void onClick(View view) {
            String author = authorTextView.getText().toString();
            String title = titleTextView.getText().toString();
            String messageContents = view.getContext()
                    .getString(R.string.reddit_share_message_template, author, title);
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT, messageContents);
            intent.setType("plain/text");
            // First we verify if the intent can be resolved.
            if(null != intent.resolveActivity(view.getContext().getPackageManager())) {
                view.getContext().startActivity(Intent.createChooser(intent,
                        view.getContext().getString(R.string.app_chooser_title)));
            }
            else {
                Context context = itemView.getContext();
                Toast.makeText(context, R.string.share_reddit_post_no_apps_available_error,
                        Toast.LENGTH_LONG).show();
            }
        }

        private void setAuthorFont() {
            Typeface authorFont = Typeface.createFromAsset(itemView.getContext().getAssets(),
                    GeneralConstants.BEBASNEUE_FONT_PATH);
            authorTextView.setTypeface(authorFont);
        }

        private void animatePost(){
            itemView.startAnimation(AnimationUtils.loadAnimation(itemView.getContext(), R.anim.move_in_from_right));
        }
    }

    @Override
    public RedditPostAdapter.RedditPostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.reddit_post, parent, false);
        return new RedditPostHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(RedditPostAdapter.RedditPostHolder holder, int position) {
        RedditPost post = posts.get(position);
        holder.bindRedditPost(post);
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    public void setNewPostList(List<RedditPost> newPosts) {
        posts = newPosts;
        notifyDataSetChanged();
    }

}
