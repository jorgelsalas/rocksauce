package com.example.jorge.jorgesalas_androidcodechallenge.rest;

import com.example.jorge.jorgesalas_androidcodechallenge.models.RedditPost;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Interface that describes available endpoints in the Reddit service.
 */
public interface RedditService {

    /**
     * Fetches reddit post information from the specified subcategory.
     *
     * @param subcategory The subcategory from which posts will be retrieved.
     * @return A list of posts pertaining to the specified subcategory.
     */
    @GET("{subcategory}/.json")
    Call<List<RedditPost>> fetchPostFromSubcategory(@Path("subcategory") String subcategory);
}
