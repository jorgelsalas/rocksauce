package com.example.jorge.jorgesalas_androidcodechallenge.rest;

/**
 * APIConstants used by the REST API client.
 */
public class APIConstants {

    public final static String BASE_REDDIT_URL = "http://www.reddit.com/r/";
    public final static String DATA_TAG = "data";
    public final static String REDDIT_POST_PARENT_TAG = "children";
    public final static String FUNNY_SUBCATEGORY = "funny";
    public final static int SUCCESS_CODE = 200;
}
