package com.example.jorge.jorgesalas_androidcodechallenge.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapterFactory;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Rest API client that will query reddit to acquire subreddit post information.
 */
public class APIClient {

    private static RedditService redditService = null;

    private APIClient() {

    }

    public static RedditService getRedditServiceInstance() {
        if (null == redditService) {
            Gson gson = new GsonBuilder()
                    .registerTypeAdapterFactory((TypeAdapterFactory) new ItemTypeAdapterFactory())
                    .create();

            Retrofit retrofit =  new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .baseUrl(APIConstants.BASE_REDDIT_URL)
                    .build();
            redditService = retrofit.create(RedditService.class);
        }
        return redditService;
    }
}
